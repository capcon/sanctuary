﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Sanctuary;

namespace SanctuaryWpfHost
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            Bootstrapper.Bootstrap();

            var viewModel = new MainWindowViewModel(NancyHost.Port);

            var mainWindow = new MainWindow {DataContext = viewModel};
            mainWindow.Show();
        }

        private void App_OnExit(object sender, ExitEventArgs e)
        {
            Bootstrapper.Shutdown();
        }
    }
}
