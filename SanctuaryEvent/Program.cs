﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using Sanctuary;
using Sanctuary.Bus;
using Sanctuary.Command;
using Sanctuary.Domain;
using Sanctuary.Infrastructure;
using Sanctuary.ReadModel;
using SanctuaryWpfHost;

namespace SanctuaryEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            Bootstrapper.Bootstrap();
            try
            {
                Console.WriteLine("Running on {0}", NancyHost.Port);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
            }
            finally
            {
                Bootstrapper.Shutdown();
            }
        }
    }
}
