﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary.Infrastructure
{
    public class HostInfo
    {
        public string WebContentRoot { get; set; }
    }
}
