﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Conventions;
using Sanctuary.Infrastructure;

namespace Sanctuary
{
    public class NancyBootstrapper : DefaultNancyBootstrapper
    {
        private readonly HostInfo _hostInfo;

        public NancyBootstrapper(HostInfo hostInfo)
        {
            _hostInfo = hostInfo;
        }

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            nancyConventions.ViewLocationConventions.Clear();
            nancyConventions.ViewLocationConventions.Add((view, param, context) => view);

            var rootPath = RootPathProvider.GetRootPath();
            var directories = Directory.GetDirectories(rootPath);
            foreach (var subDirectoryPath in directories)
            {
                var directoryName = Path.GetFileNameWithoutExtension(subDirectoryPath);
                if (!string.IsNullOrEmpty(directoryName))
                {
                    nancyConventions.StaticContentsConventions.Add(
                        StaticContentConventionBuilder.AddDirectory(directoryName, subDirectoryPath));
                }
            }

            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("app", "."));
            base.ConfigureConventions(nancyConventions);

            //nancyConventions.ViewLocationConventions.Add((viewName, model, context) => $"bin/{viewName}");
            //nancyConventions.ViewLocationConventions.Add((viewName, model, context) => $"content/{viewName}");
            //nancyConventions.ViewLocationConventions.Add((viewName, model, context) => $"bin/content/{viewName}");

            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("css", "content/css"));
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("scripts", "content/scripts"));
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("src", "content/src"));

            //nancyConventions.StaticContentsConventions.Clear();
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("src", "content/src", "js", "html"));
            //nancyConventions.StaticContentsConventions.Add((ctx, root) =>
            //{
            //    return null;
            //});
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("content", "bin/content"));
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("content/css", "bin/content/css"));
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("css", "bin/content/css"));
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("fonts", "bin/content/fonts"));
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("app", "."));
            //nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("app/scripts", "scripts"));
        }

        protected override IRootPathProvider RootPathProvider => new CustomRootPathProvider(_hostInfo);
    }
}
