using System;
using Sanctuary.Events;

namespace Sanctuary.Infrastructure
{
    public class PersistentEvent
    {
        public PersistentEvent(Type aggreagType, Guid aggregateId, int sequence, Event @event)
        {
            AggregateType = aggreagType;
            AggregateId = aggregateId;
            Sequence = sequence;
            Event = @event;
            EventType = @event.GetType();
        }

        public Type AggregateType { get; }
        public Guid AggregateId { get; }
        public Type EventType { get; }
        public int Sequence { get; }
        public Event Event { get; }

    }
}