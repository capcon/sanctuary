import {Routes, RouterModule} from '@angular/router'
import {EventListComponent} from "./event-list.component"

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/events',
        pathMatch: 'full'
    },
    {
        path: 'events',
        component: EventListComponent
    },
    {
        path: 'events/:key',
        component: EventListComponent
    },
];

export const routing = RouterModule.forRoot(appRoutes);