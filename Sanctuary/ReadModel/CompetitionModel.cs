﻿using System;
using System.Collections.Generic;

namespace Sanctuary.ReadModel
{
    public class CompetitionModel
    {
        public CompetitionModel(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public string Name { get; set; }

        public List<CompetitionAthlete> Athletes = new List<CompetitionAthlete>(); 
    }
}