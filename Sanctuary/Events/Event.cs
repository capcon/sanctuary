using System;
using Sanctuary.Domain;
using Sanctuary.Domain.Framework;

namespace Sanctuary.Events
{
    public abstract class Event
    {
        protected Event(Guid aggregateId)
        {
            AggregateId = aggregateId;
        }

        protected Event(AggregateRoot aggregateRoot) : this(aggregateRoot.Id)
        {
        }

        public Guid AggregateId { get; }
    }
}