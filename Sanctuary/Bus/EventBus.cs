﻿using System.Collections.Generic;
using Sanctuary.Events;
using Sanctuary.ReadModel;

namespace Sanctuary.Bus
{
    public class EventBus
    {
        public static readonly EventBus Instance = new EventBus();
        private readonly List<object> _listeners = new List<object>();

        public void Listner(object listener)
        {
            _listeners.Add(listener);
        }

        public void Publish(IEnumerable<Event> @events)
        {
            foreach (var @event in events)
            {
                var handlesType = typeof (Handles<>).MakeGenericType(@event.GetType());
                foreach (var listener in _listeners)
                {
                    var interestedInEvent = handlesType.IsInstanceOfType(listener);
                    if (interestedInEvent)
                    {
                        var handleMethod = listener.GetType().GetMethod("Handle", new[] {@event.GetType()});
                        handleMethod.Invoke(listener, new object[] {@event});
                    }
                }
            }
        }
    }
}