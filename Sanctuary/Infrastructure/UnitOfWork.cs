﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sanctuary.Bus;
using Sanctuary.Domain.Framework;
using Sanctuary.Events;

namespace Sanctuary.Infrastructure
{
    public class UnitOfWork : IDisposable
    {
        private readonly List<Tuple<IAggregateRoot, Event>> _pendingEvents = new List<Tuple<IAggregateRoot, Event>>();
        [ThreadStatic] private static UnitOfWork _current;

        public static UnitOfWork Start()
        {
            if (_current == null)
            {
                _current = new UnitOfWork();
            }
            return _current;
        }

        public static UnitOfWork Current => _current;

        public void Dispose()
        {
            _pendingEvents.Clear();
        }

        public void Complete()
        {
            EventStore.Instance.Persist(_pendingEvents);
            EventBus.Instance.Publish(_pendingEvents.Select(x => x.Item2));
            _pendingEvents.Clear();
        }

        public void Add(IAggregateRoot aggregateRoot, Event @event)
        {
            _pendingEvents.Add(Tuple.Create(aggregateRoot, @event));
        }
    }
}