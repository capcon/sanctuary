﻿using System;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Web;
using Microsoft.Owin.Extensions;
using Nancy;
using Nancy.Owin;
using Owin;
using Sanctuary.Infrastructure;

namespace Sanctuary
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //HACK: Need to figure out the propert bootstrapping process to allow the host to provide some config
            //information.  Ideally info stuck into a container that can be pulled out by the other classes
            //var appPath = HttpRuntime.AppDomainAppPath;
            var appPath = AppDomain.CurrentDomain.BaseDirectory;
            string contentPath;
            if (appPath.Contains("SanctuaryWeb"))
            {
                contentPath = Path.GetFullPath(Path.Combine(appPath, @"..\sanctuary-web"));
            }
            else
            {
                contentPath = Path.GetFullPath(Path.Combine(appPath, @"..\..\..\sanctuary-web"));
            }

            var hostInfo = new HostInfo
            {
                WebContentRoot = contentPath
            };

            app.UseNancy(options =>
            {
                options.PassThroughWhenStatusCodesAre(HttpStatusCode.NotFound);
                options.Bootstrapper = new NancyBootstrapper(hostInfo);
            });
            app.UseStageMarker(PipelineStage.MapHandler);

            Bootstrapper.Bootstrap(startNancy: false);
        }
    }
}
