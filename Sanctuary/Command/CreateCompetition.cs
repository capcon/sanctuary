﻿using System;
using Sanctuary.Domain;
using Sanctuary.Infrastructure;

namespace Sanctuary.Command
{
    public class CreateCompetition
    {
        private readonly Guid _guid;
        private readonly string _name;

        public CreateCompetition(Guid guid, string name)
        {
            _guid = guid;
            _name = name;
        }

        public void Execute()
        {
            using (var uow = UnitOfWork.Start())
            {
                var competition = new Competition(_guid, _name);
                uow.Complete();
            }
        }
    }
}