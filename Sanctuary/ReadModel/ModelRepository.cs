﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary.ReadModel
{
    public class ModelRepository
    {
        public static readonly ModelRepository Instance = new ModelRepository();
        private readonly Dictionary<Type, List<object>> _modelsByType = new Dictionary<Type, List<object>>();

        public void Add<T>(T model)
        {
            List<object> models;
            if (!_modelsByType.TryGetValue(typeof(T), out models))
            {
                models = new List<object>();
                _modelsByType[typeof (T)] = models;
            }
            models.Add(model);
        }

        public T Get<T>(Func<T, bool> condition)
        {
            List<object> models;
            if (!_modelsByType.TryGetValue(typeof(T), out models))
            {
                return default(T);
            }

            return models.OfType<T>().FirstOrDefault(condition);
        }

        public IEnumerable<T> GetAll<T>()
        {
            List<object> models;
            if (!_modelsByType.TryGetValue(typeof(T), out models))
            {
                return Enumerable.Empty<T>();
            }

            return models.OfType<T>().ToList();
        }
    }
}
