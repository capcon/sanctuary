﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sanctuary.Domain;
using Sanctuary.Infrastructure;
using CompetitionAthlete = Sanctuary.ReadModel.CompetitionAthlete;

namespace Sanctuary.Command
{
    public class AddAthlete
    {
        public Guid EventId { get; set; }

        public CompetitionAthlete Athlete { get; set; }

        public void Execute()
        {
            using (var uow = UnitOfWork.Start())
            {
                var competition = Repository.Get<Competition>(EventId);
                competition.AddAthlete(Athlete.Name);

                uow.Complete();
            }
        }
    }
}
