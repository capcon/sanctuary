﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Input;
using SanctuaryWpfHost.Annotations;

namespace SanctuaryWpfHost
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private string _url;
        private Uri _uri;
        private readonly Icon _icon;
        private readonly ICommand _urlClicked;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public MainWindowViewModel()
        {
        }

        public MainWindowViewModel(int port)
        {
            var hostName = Dns.GetHostName();
            var ipAddressDetails = Dns.GetHostEntry(hostName);
            var ipAddress = ipAddressDetails.AddressList.First(x => x.AddressFamily == AddressFamily.InterNetwork).ToString();
            Url = $"http://{ipAddress}:{port}";
            Uri = new Uri(Url);

            _urlClicked = new RelayCommand(_ => Process.Start(Url));

            //var bitmap = new Bitmap("sanctuary-image.png");
            //_icon = Icon.FromHandle(bitmap.GetHicon());

            Process.Start(Url);
        }

        public string Url
        {
            get { return _url; }
            set
            {
                if (value == _url) return;
                _url = value;
                OnPropertyChanged();
            }
        }

        public Uri Uri
        {
            get { return _uri; }
            set
            {
                if (Equals(value, _uri)) return;
                _uri = value;
                OnPropertyChanged();
            }
        }

        public Icon Icon
        {
            get { return _icon; }
        }

        public ICommand UrlClicked
        {
            get { return _urlClicked; }
        }
    }
}
