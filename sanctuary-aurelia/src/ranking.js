﻿import {CompetitorRanking} from './competitorRanking'

export class Ranking {
    constructor() {
        this.rankings = [new CompetitorRanking("John", 12), new CompetitorRanking("Dave", 10)];
    }
}