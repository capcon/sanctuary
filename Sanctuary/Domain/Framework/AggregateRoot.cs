using System;
using System.Collections.Generic;
using System.Reflection;
using Sanctuary.Command;
using Sanctuary.Events;
using Sanctuary.Infrastructure;

namespace Sanctuary.Domain.Framework
{
    public class AggregateRoot : IAggregateRoot
    {
        public Guid Id { get; protected set; }

        protected void ApplyChange(Event @event, bool replaying = false)
        {
            var applicationMethod = GetType()
                .GetMethod("Apply", BindingFlags.NonPublic | BindingFlags.Instance, null, new[] {@event.GetType()}, null);
            applicationMethod.Invoke(this, new object[] { @event });

            if (!replaying)
            {
                UnitOfWork.Current.Add(this, @event);
            }
        }

        void IAggregateRoot.ApplyHistory(Guid id, IEnumerable<Event> eventHistory)
        {
            Id = id;
            foreach (var @event in eventHistory)
            {
                ApplyChange(@event, true);
            }
        }
    }
}