﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NEventStore;
using Sanctuary.Bus;
using Sanctuary.ReadModel;
using SanctuaryWpfHost;

namespace Sanctuary
{
    public static class Bootstrapper
    {
        private static IDisposable _nancyFxHost;

        public static void Bootstrap(bool startNancy = true)
        {
            var competitionModelGenerator = new CompetitionModelGenerator();
            EventBus.Instance.Listner(competitionModelGenerator);

            if (startNancy)
            {
                _nancyFxHost = NancyHost.Start();
            }
        }

        public static void Shutdown()
        {
            _nancyFxHost?.Dispose();
        }
    }
}
