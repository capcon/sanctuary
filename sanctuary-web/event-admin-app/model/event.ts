import { Athlete } from './athlete'

export class Event
{
    id: string;
    name: string = '';
    athletes: Athlete[] = [];
}
