import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import {AppComponent} from "./app.component";
import {EventListComponent} from "./event-list.component"
import {EventService} from './event.service'
import {EventDetailComponent} from './event-detail.component'
import {routing} from "./app.routing"
import {Http, HttpModule} from "@angular/http";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing
    ],
    declarations: [
        AppComponent,
        EventListComponent,
        EventDetailComponent
    ],
    providers: [EventService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
