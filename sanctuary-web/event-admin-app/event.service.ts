import { Injectable } from '@angular/core'
import {Http, Response, Headers} from '@angular/http'
import { Event } from './model/event'
import 'rxjs/add/operator/toPromise'
import { Model } from './model-functions'
import {Athlete} from "./model/athlete";

@Injectable()
export class EventService {
    constructor(private http : Http) {
    }

    getAll() : Promise<Event[]> {
        return this.http.get('api/event')
            .toPromise()
            .then(response => response.json() as Event[])
            .catch(this.handleError);
    }

    get(id : string) : Promise<Event> {
        return this.http.get('/api/event/' + id)
            .toPromise()
            .then(response => response.json() as Event)
            .catch(this.handleError);
    }

    save(event : Event) : Promise<Response> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        if (Model.isNew(event)) {
            return this.http
                .post('api/event/create', JSON.stringify(event), {headers: headers})
                .toPromise()
                .catch(this.handleError);
        }
        else {
            return this.http
                .post('api/event/update', JSON.stringify(event), {headers: headers})
                .toPromise()
                .catch(this.handleError);
        }
    }

    addAthlete(event : Event, athlete : Athlete) : Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .post('api/event/addAthlete', JSON.stringify({ eventId : event.id, athlete : athlete}), {headers: headers})
            .toPromise()
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('An error occurred in event service ', error);
        return Promise.reject(error.message || error);
    }
}