﻿using System;
using System.Collections.Generic;
using Sanctuary.Domain.Framework;
using Sanctuary.Events;

namespace Sanctuary.Domain
{
    public class Competition : AggregateRoot
    {
        private string _name;
        private List<CompetitionAthlete> _athletes = new List<CompetitionAthlete>();

        public Competition()
        {
        }

        public Competition(Guid id, string name)
        {
            Id = id;
            ApplyChange(new CompeitionCreated(this, name));
        }

        private void Apply(CompeitionCreated compeitionCreated)
        {
            _name = compeitionCreated.Name;
        }

        public void UpdateName(string newName)
        {
            ApplyChange(new CompetitionNameChanged(this, _name, newName));
        }

        private void Apply(CompetitionNameChanged nameChanged)
        {
            _name = nameChanged.NewName;
        }

        private void Apply(AthleteAdded athleteAdded)
        {
            _athletes.Add(new CompetitionAthlete(athleteAdded.AthleteName));
        }

        public void AddAthlete(string name)
        {
            ApplyChange(new AthleteAdded(this, name));
        }
    }
}