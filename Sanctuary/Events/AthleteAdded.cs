﻿using Sanctuary.Domain;

namespace Sanctuary.Events
{
    public class AthleteAdded : Event
    {
        private readonly string _athleteName;

        public AthleteAdded(Competition aggregateRoot, string athleteName) : base(aggregateRoot)
        {
            _athleteName = athleteName;
        }

        public string AthleteName
        {
            get { return _athleteName; }
        }
    }
}