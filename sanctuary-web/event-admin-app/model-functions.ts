// Utility functions for working with model classes
export module Model {
    export function isNew(model: any): boolean {
        return !model.id;
    }
}
