﻿using System;
using System.Collections.Generic;
using Sanctuary.Events;

namespace Sanctuary.Domain.Framework
{
    public interface IAggregateRoot
    {
        Guid Id { get; }
        void ApplyHistory(Guid id, IEnumerable<Event> eventHistory);
    }
}