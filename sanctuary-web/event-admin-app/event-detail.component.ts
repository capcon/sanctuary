import { Component, Input, Output, EventEmitter , OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { NgModel } from '@angular/forms'
import { Event } from './model/event'
import {EventService} from "./event.service";
import {Athlete} from "./model/athlete";
import { Model} from './model-functions'

@Component({
    selector: "event-detail",
    templateUrl: "event-admin-app/event-detail.component.html",
})
export class EventDetailComponent implements OnInit {

    @Input() currentEvent : Event;
    @Output() saved = new EventEmitter();
    newAthleteName : string = '';
    active : boolean = true;

    constructor(private eventService : EventService, private route : ActivatedRoute) {
    }

    ngOnInit() {
        this.currentEvent = new Event();
        this.route.params.forEach((params: Params) => {
            if (params['key'] !== undefined) {
                let eventKey = params['key'];
                if (eventKey === 'new')
                {
                    this.clearEvent();
                }
                else {
                    this.getEvent(eventKey);
                }
            }
            else {
                this.clearEvent();
            }
        });
    }

    private clearEvent() {
        this.currentEvent = new Event();
        this.resetForm();
    }

    private getEvent(eventKey: any) {
        this.eventService.get(eventKey).then(x => {
            this.currentEvent = x;
            this.resetForm();
        });
    }

    private resetForm() {
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }

    newEvent() : boolean {
        return Model.isNew(this.currentEvent);
    }

    saveEvent() {
        this.eventService.save(this.currentEvent)
            .then(x => {
                var eventGuid = x.json().guid;
                this.saved.emit(eventGuid);
                this.getEvent(eventGuid);
            });
    }

    addAthlete() {
        var athlete = new Athlete();
        athlete.name = this.newAthleteName;
        this.eventService.addAthlete(this.currentEvent, athlete)
            .then(_ => {
                this.newAthleteName = '';
                this.saved.emit(this.currentEvent.id);
                this.getEvent(this.currentEvent.id);
            });
    }
}