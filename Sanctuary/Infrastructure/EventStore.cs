using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Sanctuary.Domain.Framework;
using Sanctuary.Events;

namespace Sanctuary.Infrastructure
{
    public class EventStore
    {
        public static readonly EventStore Instance = new EventStore();
        private static int _nextEventSequence = 0;
        private readonly List<PersistentEvent> _events = new List<PersistentEvent>();

        public void Persist(IEnumerable<Tuple<IAggregateRoot, Event>> events)
        {
            var fullEvents = events.Select(
                x =>
                    new PersistentEvent(x.Item1.GetType(), x.Item1.Id, Interlocked.Increment(ref _nextEventSequence),
                        x.Item2)).ToList();
            _events.AddRange(fullEvents);
        }

        public IEnumerable<Event> GetEvents(Type type, Guid id)
        {
            return _events.Where(x => x.AggregateType == type && x.AggregateId == id).OrderBy(x => x.Sequence).Select(x => x.Event).ToList();
        }
    }
}