import { Component, OnInit } from '@angular/core'
import { AlertComponent } from 'ng2-bootstrap/ng2-bootstrap'
import {Router, ActivatedRoute, Params} from '@angular/router'
import { Event } from './model/event'
import {EventService} from "./event.service";
import {EventDetailComponent} from './event-detail.component'
import { Model } from './model-functions'

@Component({
    selector: 'event-list',
    templateUrl: 'event-admin-app/event-list.component.html',
    directives: [ AlertComponent, EventDetailComponent ]
})
export class EventListComponent implements OnInit {
    events: Event[] = [];

    constructor(private eventService : EventService, private router : Router, private route : ActivatedRoute) {
    }

    text = "No Events";
    selectedEventKey : string = '';

    eventSelected(event: Event) {
        var link;
        if (Model.isNew(event))
        {
            link = [ '/events', 'new' ]
        }
        else
        {
            link = [ '/events', event.id ]
        }
        this.router.navigate(link);
    }

    myIsNew() {
        return this.selectedEventKey === '' || this.selectedEventKey === 'new';
    }

    eventSaved(id : string) {
        if ( id === this.selectedEventKey) {
            this.getEvents();
        }
        else {
            this.router.navigate([ '/events', id ]);
        }
    }

    ngOnInit(): void {
        this.events = [];
        this.selectedEventKey = '';
        this.route.params.forEach((params: Params) => {
            if (params['key'] !== undefined) {
                this.selectedEventKey = params['key'];
            }
            else {
                this.selectedEventKey = '';
                this.router.navigate([ '/events', 'new' ]);
            }
            this.getEvents();
        });
    }

    private getEvents() : Promise<Event[]> {
        return this.eventService.getAll()
            .then(x => {
                this.events = x;
            });
    }
}