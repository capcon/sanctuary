import { Component } from '@angular/core'

@Component({
    selector: 'main-app',
    templateUrl: 'public-app/app.component.html'
})
export class AppComponent {
    title = 'Event Dashboard'
}