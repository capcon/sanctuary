using System;
using Sanctuary.Domain.Framework;

namespace Sanctuary.Infrastructure
{
    public class Repository
    {
        public static T Get<T>(Guid id) where T : IAggregateRoot, new()
        {
            var events = EventStore.Instance.GetEvents(typeof (T), id);
            var aggregate = new T();
            aggregate.ApplyHistory(id, events);
            return aggregate;
        }
    }
}