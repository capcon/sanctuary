﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Conventions;
using Nancy.ModelBinding;
using Nancy.Responses;
using Sanctuary.Command;

namespace Sanctuary
{
    public class FrontEndModule : NancyModule
    {
        public FrontEndModule()
        {
            //Get["/"] = _ => new RedirectResponse("app/index.html");
            Get["/"] = _ => View["public"];
            Get["/app"] = _ => View["event-admin"];
            Get["/app/{clientRoute*}"] = _ => View["event-admin"];
            //Get["/app"] = _ => View["content/public-app.html"];
            //Get["/"] = _ => View["content/public-app.html"];
            //Get["/app"] = _ => "Seperate Module";
            //Get["/view"] = _ => View["sample.html"];
            //Get["/ranking"] = _ => View["ranking.html"];
            //Get["/second"] = _ => View["spa2.html"];

            //Get["/rankings"] = _ => new Rankings {EventCount = 5};
        }
    }
}
