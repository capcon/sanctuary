﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.ModelBinding;
using Sanctuary.Command;
using Sanctuary.ReadModel;

namespace Sanctuary
{
    public class WebApiModule : NancyModule
    {
        public WebApiModule()
        {
            Get["api/event"] = _ => ModelRepository.Instance.GetAll<CompetitionModel>();

            Get["api/event/{id}"] = parameters =>
            {
                var guid = new Guid(parameters["id"]);
                return ModelRepository.Instance.Get<CompetitionModel>(x => x.Id == guid);
            };

            Post["api/event/create"] = parameters =>
            {
                var eventDto = this.Bind<EventDto>();

                var guid = Guid.NewGuid();
                var createCompetition = new CreateCompetition(guid, eventDto.Name);
                createCompetition.Execute();

                return Response.AsJson(new { Guid = guid });
            };

            Post["api/event/update"] = parameters =>
            {
                var eventDto = this.Bind<EventUpdateDto>();

                var updateCompetitionName = new UpdateCompetitionName(eventDto.Id, eventDto.Name);
                updateCompetitionName.Execute();

                return Response.AsJson(new { Guid = eventDto.Id });
            };

            Post["/api/event/addAthlete"] = parameters =>
            {
                var addAthlete = this.Bind<AddAthlete>();

                addAthlete.Execute();

                return HttpStatusCode.Accepted;
            };
        }
    }
}
