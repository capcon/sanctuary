﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using Sanctuary;

namespace SanctuaryWpfHost
{
    public class NancyHost
    {
        public const int Port = 8080;

        public static IDisposable Start()
        {
            var url = $"http://+:{Port}";

            return WebApp.Start<Startup>(url);
        }
    }
}
