﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Sanctuary.Infrastructure;

namespace Sanctuary
{
    public class CustomRootPathProvider : IRootPathProvider
    {
        private readonly HostInfo _hostInfo;

        public CustomRootPathProvider(HostInfo hostInfo)
        {
            _hostInfo = hostInfo;
        }

        public string GetRootPath()
        {
            return _hostInfo.WebContentRoot;
        }
    }
}
