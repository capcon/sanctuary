using System;
using System.Collections.Generic;
using Sanctuary.Bus;
using Sanctuary.Domain;
using Sanctuary.Events;

namespace Sanctuary.ReadModel
{
    public class CompetitionModelGenerator : Handles<CompeitionCreated>, Handles<CompetitionNameChanged>, Handles<AthleteAdded>
    {
        private readonly Dictionary<Guid, CompetitionModel> _competitionsById = new Dictionary<Guid, CompetitionModel>(); 

        public void Handle(CompeitionCreated @event)
        {
            var competitionModel = new CompetitionModel(@event.AggregateId)
            {
                Name = @event.Name
            };
            _competitionsById[competitionModel.Id] = competitionModel;
            ModelRepository.Instance.Add(competitionModel);
        }

        public void Handle(CompetitionNameChanged @event)
        {
            CompetitionModel competitionModel;
            if (_competitionsById.TryGetValue(@event.AggregateId, out competitionModel))
            {
                competitionModel.Name = @event.NewName;
            }
        }

        public object Model(Guid competiionId)
        {
            CompetitionModel model;
            _competitionsById.TryGetValue(competiionId, out model);
            return model;
        }

        public void Handle(AthleteAdded @event)
        {
            CompetitionModel competitionModel;
            if (_competitionsById.TryGetValue(@event.AggregateId, out competitionModel))
            {
                competitionModel.Athletes.Add(new CompetitionAthlete { Name = @event.AthleteName});
            }
        }
    }
}