using Sanctuary.Events;

namespace Sanctuary.Bus
{
    public interface Handles<T> where T : Event
    {
        void Handle(T @event);
    }
}