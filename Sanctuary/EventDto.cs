﻿using System;

namespace Sanctuary
{
    public class EventDto
    {
        public string Name { get; set; }
    }

    public class EventUpdateDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}