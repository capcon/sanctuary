﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sanctuary.Bus;
using Sanctuary.Command;
using Sanctuary.ReadModel;
using Xunit;

namespace SanctuaryTests
{
    public class CompetitionTests
    {
        [Fact]
        public void EventTest()
        {
            var competitionModelGenerator = new CompetitionModelGenerator();
            EventBus.Instance.Listner(competitionModelGenerator);

            var competiionId = Guid.NewGuid();
            var createCompetitionCommand = new CreateCompetition(competiionId, "First comp");
            createCompetitionCommand.Execute();

            new UpdateCompetitionName(competiionId, "Updated name").Execute();

            var competitionModel = ModelRepository.Instance.Get<CompetitionModel>(x => x.Id == competiionId);
        }
    }
}
