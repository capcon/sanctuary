using Sanctuary.Domain;

namespace Sanctuary.Events
{
    public class CompetitionNameChanged : Event
    {
        public string OldName { get; }
        public string NewName { get; }

        public CompetitionNameChanged(Competition aggregateRoot, string oldName, string newName) : base(aggregateRoot)
        {
            OldName = oldName;
            NewName = newName;
        }
    }
}