﻿using Sanctuary.Domain;

namespace Sanctuary.Events
{
    public class CompeitionCreated : Event
    {
        public CompeitionCreated(Competition competition, string name) : base(competition)
        {
            Name = name;
        }

        public string Name { get; }
    }
}