using System;
using Sanctuary.Domain;
using Sanctuary.Infrastructure;

namespace Sanctuary.Command
{
    public class UpdateCompetitionName
    {
        private readonly Guid _competitionId;
        private readonly string _newName;

        public UpdateCompetitionName(Guid competitionId, string newName)
        {
            _competitionId = competitionId;
            _newName = newName;
        }

        public void Execute()
        {
            using (var uow = UnitOfWork.Start())
            {
                var competition = Repository.Get<Competition>(_competitionId);
                competition.UpdateName(_newName);
                uow.Complete();
            }
        }
    }
}